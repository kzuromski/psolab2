package com.example.pso;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import java.text.DecimalFormat;

public class FormActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_activity);
        FormActivity formActivity = this;
        Button clickButton = (Button) findViewById(R.id.button);
        clickButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText weight = (EditText)findViewById(R.id.editTex1);
                EditText height = (EditText)findViewById(R.id.editText2);
                DecimalFormat df = new DecimalFormat("#.##");
                double bmi = calculateBMI(Double.parseDouble(weight.getText().toString()), Double.parseDouble(height.getText().toString()));
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                i.putExtra("BMI", df.format(bmi));
                startActivity(i);
                finish();
            }
        });
        Button webButton = (Button) findViewById(R.id.button2);
        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.addCategory(Intent.CATEGORY_BROWSABLE);
                i.setData(Uri.parse("https://pl.wikipedia.org/wiki/Wska%C5%BAnik_masy_cia%C5%82a"));
                startActivity(i);
            }
        });
    }

    private double calculateBMI(Double weight, Double height) {
        double heightInMeters = height / 100;
        return weight / (heightInMeters * heightInMeters);
    }
}
