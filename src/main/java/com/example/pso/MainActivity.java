package com.example.pso;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView img = findViewById(R.id.imageView);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(MainActivity.this, FormActivity.class);
                startActivity(in);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent i = getIntent();
        i.getExtras();
        if(i.hasExtra("BMI")) {
            String bmi = i.getExtras().getString("BMI");
            Toast.makeText(MainActivity.this,
                    "BMI: " + bmi, Toast.LENGTH_LONG).show();
            TextView bmiResult = findViewById(R.id.textView4);

        }
    }
}
